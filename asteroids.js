﻿/* Author: Gennaro Rubino
 * Date: 12/12/2019
 * Assignment: Final Project
 *
 * Source of Tutorial: https://www.youtube.com/watch?v=H9CSWMxJx84
 * Visual References: https://www.youtube.com/watch?v=9Ydu8UhIjeU
 *                    https://www.youtube.com/watch?v=eLKudh3vvsU
 * Custom Font: https://www.dafont.com/hyperspace.font
 *
 * Followed along with tutorial found on youtube, but didn't follow it all the way through
 * to the end.
 *
 * Changed ship geometry to have a semicircle rear to resemble the original atari design.
 *
 * Changed thruster size and color to resemble the original atari design.
 *
 * Created a glowing effect around all strokes so that it resembles original arcade machine
 * display.
 *
 * Created custom methods to improve the hit boxes around the asteroids and the ship to
 * match their geometries, but did not polish the design for the lasers to detect those same
 * hitboxes.
 *
 * Created particle effects for destroyed asteroids.
 *
 * Imported custom fonts to resemble original design.
 *
 * Made sure the game filled the window and is dynamic with the window in a 19:6 aspect ratio
 *
 * Refactored methods and created class prototypes to clean up code compaired to the tutorial I followed
 *
 * Created a particle method for the ship explosion which features particles of random length spreading out and rotating
 *
 */

const COPYRIGHT_MSG = "© ATARI 1979";
const FPS = 30; // frames per second
const FRICTION = 0.1; // friction coefficient of space (0 = no friction, 1 = lots of friction)
const GAME_LIVES = 3; // starting number of lives
const LASER_DIST = 0.6; // max distance laser can travel as fraction of screen width
const LASER_EXPLODE_DUR = 0.1; // duration of the lasers' explosion in seconds
const LASER_MAX = 10; // maximum number of lasers on screen at once
const LASER_SPD = 500; // speed of lasers in pixels per second
const ROID_JAG = 0.4; // jaggedness of the asteroids (0 = none, 1 = lots)
const ROID_NUM = 3; // starting number of asteroids
const ROID_SIZE = 100; // starting size of asteroids in pixels
const ROID_SPD = 75; // max starting speed of asteroids in pixels per second
const ROID_VERT = 10; // average number of vertices on each asteroid
const SHIP_BLINK_DUR = 0.1; // duration in seconds of a single blink during ship's invisibility
const SHIP_SIZE = 30; // ship height in pixels
const SHIP_THRUST = 10; // acceleration of the ship in pixels per second per second
const SHIP_TURN_SPD = 360; // turn speed in degrees per second
const SHOW_BOUNDING = false; // show or hide collision bounding
const SHOW_CENTRE_DOT = false; // show or hide ship's centre dot
const TEXT_FADE_TIME = 2.5; // text fade time in seconds
const TEXT_SIZE = 40; // text font height in pixels
const ROID_PTS_LRG = 20; //points scored for larger astroid
const ROID_PTS_MED = 50; //points scored for medium astroid
const ROID_PTS_SML = 100; //points scored for small asteroid
const SAVE_KEY_SCORE = "highScoreSave"; // save key for locally storing high score

/** @type {HTMLCanvasElement} */
var canv = document.getElementById("gameCanvas");
var ctx = canv.getContext("2d");

var num_particles = 8;

//create a glow for anything rendered to screen
ctx.shadowBlur = 15;
ctx.shadowColor = "white";

// set up the game parameters
var level, lives, roids, score, highScore, ship, text, textAlpha;
newGame();

// set up event handlers
document.addEventListener("keydown", keyDown);
document.addEventListener("keyup", keyUp);

// set up the game loop
setInterval(update, 1000 / FPS);

// ============== Particle object prototype =========================//
//Particle object with random starting position, velocity and color
var Particle = function (x, y)
{
    this.x = x;
    this.y = y;
    this.r = 0;
    this.vx = 4 * Math.random() - 2;
    this.vy = 4 * Math.random() - 2;
    this.vr = Math.random() * 0.1;
    this.particleType = "rect";
    this.particleLength = 4;
    this.life = 50;
}
//Adding two methods
Particle.prototype.Draw = function (ctx)
{
    ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
    if (this.particleType == "rect")
    {
        ctx.fillRect(this.x, this.y, 4, this.particleLength);
    }
    else if (this.particleType == "line")
    {
        ctx.moveTo(this.x - (Math.cos(this.r) * this.particleLength),
                   this.y - (Math.sin(this.r) * this.particleLength));
        ctx.lineTo(this.x + (Math.cos(this.r) * this.particleLength),
                   this.y + (Math.sin(this.r) * this.particleLength));
        ctx.strokeStyle = '#ffffff';
        ctx.stroke();
    }
}
Particle.prototype.Update = function ()
{
    if (this.particleType == "rect")
    {
        this.x += this.vx;
        this.y += this.vy;
    }
    else if (this.particleType = "line")
    {
        this.x += this.vx; //* 2;
        this.y += this.vy; //* 2;
        this.r += this.vr;
    }
    this.life--;
}

// ============== Ship object prototype =========================//
function Ship()
{
    this.x = canv.width / 2;
    this.y = canv.height / 2;
    this.a = 90 / 180 * Math.PI; // convert to radians
    this.r = SHIP_SIZE / 2;

    var shipInvisibilityDuration = 3; // duration of the ship's invisibility in seconds
    this.blinkNum = Math.ceil(shipInvisibilityDuration / SHIP_BLINK_DUR);
    this.blinkTime = Math.ceil(SHIP_BLINK_DUR * FPS);
    this.canShoot = true;
    this.dead = false;
    this.explodeTime = 0;
    this.particles = [];
    this.lasers = [];
    this.rot = 0;
    this.numParticles = 6;
    this.thrusting = false;
    this.thrust = {x: 0,
                   y: 0};
}
Ship.prototype.explode = function ()
{
    var shipExplodeDuration = 1.8; // duration of the ship's explosion in seconds
    this.explodeTime = Math.ceil(shipExplodeDuration * FPS);
    for (var i = 0; i < this.numParticles; i++)
    {
        var newParticle = new Particle(this.x, this.y);
        newParticle.particleType = "line";
        newParticle.particleLength = (Math.random() * 8) + 4;
        this.particles.push(newParticle);
    }
}
Ship.prototype.shootLaser = function ()
{
    // create the laser object
    if (this.canShoot && this.lasers.length < LASER_MAX)
    {
        this.lasers.push({ // from the nose of the ship
            x: this.x + 4 / 3 * this.r * Math.cos(this.a),
            y: this.y - 4 / 3 * this.r * Math.sin(this.a),
            xv: LASER_SPD * Math.cos(this.a) / FPS,
            yv: -LASER_SPD * Math.sin(this.a) / FPS,
            dist: 0,
            explodeTime: 0
        });
    }

    // prevent further shooting
    this.canShoot = false;
}
Ship.prototype.collisionDetected = function (asteroid)
{
    function orientation(pointA, pointB, pointC)
    {
        var result = 0;
        var val = (pointB.y - pointA.y) * (pointC.x - pointB.x)
            - (pointB.x - pointA.x) * (pointC.y - pointB.y);

        if (val > 0)
        {
            // Clockwise
            result = 1;
        }
        else if (val < 0)
        {
            // Counterclockwise
            result = 2;
        }
        return result;
    }

    function onSegment(pointA, pointB, pointC)
    {
        var result = false;
        if (pointB.x <= Math.max(pointA.x, pointC.x)
            && pointB.x >= Math.min(pointA.x, pointC.x)
            && pointB.y <= Math.max(pointA.y, pointC.y)
            && pointB.y >= Math.min(pointA.y, pointC.y))
        {
            result = true;
        }
        return result;
    }

    function doIntersect(pointA1, pointA2, pointB1, pointB2)
    {
        var result = false;

        var ori1 = orientation(pointA1, pointA2, pointB1);
        var ori2 = orientation(pointA1, pointA2, pointB2);
        var ori3 = orientation(pointB1, pointB2, pointA1);
        var ori4 = orientation(pointB1, pointB2, pointA2);

        if ((ori1 != ori2 && ori3 != ori4)
            || (ori1 == 0 && onSegment(pointA1, pointB1, pointA2))
            || (ori2 == 0 && onSegment(pointA1, pointB2, pointA2))
            || (ori3 == 0 && onSegment(pointB1, pointA1, pointB2))
            || (ori4 == 0 && onSegment(pointB1, pointA2, pointB2)))
        {
            result = true;
        }
        return result;
    }

    function objectsIntersect(pointsArray1, pointsArray2)
    {
        var result = false;
        for (var i = 0; i < pointsArray1.length; i++)
        {
            for (var j = 0; j < pointsArray2.length; j++)
            {
                result = doIntersect(pointsArray1[i], pointsArray1[(i + 1) % pointsArray1.length],
                    pointsArray2[j], pointsArray2[(j + 1) % pointsArray2.length]);
                if (result)
                {
                    return result;
                }
            }
        }

        return result;
    }

    var roidPoints = [];
    var a = asteroid.a;
    var r = asteroid.r;
    var x = asteroid.x;
    var y = asteroid.y;
    var offs = asteroid.offs;
    var vert = asteroid.vert;

    roidPoints.push({
        x: x + r * offs[0] * Math.cos(a),
        y: y + r * offs[0] * Math.sin(a)
    });

    for (var i = 1; i < vert; i++)
    {
        roidPoints.push({
            x: x + r * offs[i] * Math.cos(a + i * Math.PI * 2 / vert),
            y: y + r * offs[i] * Math.sin(a + i * Math.PI * 2 / vert)
        });
    }

    var pointsToCheck = [{
        x: ship.x + 4 / 3 * ship.r * Math.cos(ship.a),
        y: ship.y - 4 / 3 * ship.r * Math.sin(ship.a)
    },
    {
        x: ship.x - ship.r * (2 / 3 * Math.cos(ship.a) + Math.sin(ship.a)),
        y: ship.y + ship.r * (2 / 3 * Math.sin(ship.a) - Math.cos(ship.a))
    },
    {
        x: ship.x - ship.r * (2 / 3 * Math.cos(ship.a) - Math.sin(ship.a)),
        y: ship.y + ship.r * (2 / 3 * Math.sin(ship.a) + Math.cos(ship.a))
    }];
    result = false;
    if (objectsIntersect(roidPoints, pointsToCheck))
    {
        result = true;
    }
    return result;
}

function createAsteroidBelt() {
    roids = [];
    var x, y;
    for (var i = 0; i < ROID_NUM + level; i++)
    {
        // random asteroid location (not touching spaceship)
        do
        {
            x = Math.floor(Math.random() * canv.width);
            y = Math.floor(Math.random() * canv.height);
        } while (distBetweenPoints(ship.x, ship.y, x, y) < ROID_SIZE * 2 + ship.r);
        roids.push(newAsteroid(x, y, Math.ceil(ROID_SIZE / 2)));
    }
}

function destroyAsteroid(index)
{
    var x = roids[index].x;
    var y = roids[index].y;
    var r = roids[index].r;

    // split the asteroid in two if necessary
    if (r == Math.ceil(ROID_SIZE / 2))
    {   // large asteroid
        roids.push(newAsteroid(x, y, Math.ceil(ROID_SIZE / 4)));
        roids.push(newAsteroid(x, y, Math.ceil(ROID_SIZE / 4)));
        score += ROID_PTS_LRG;
    }
    else if (r == Math.ceil(ROID_SIZE / 4))
    {   // medium asteroid
        roids.push(newAsteroid(x, y, Math.ceil(ROID_SIZE / 8)));
        roids.push(newAsteroid(x, y, Math.ceil(ROID_SIZE / 8)));
        score += ROID_PTS_MED;
    }
    else
    {
        score += ROID_PTS_SML;
    }

    // check high score
    if (score > highScore)
    {
        highScore = score;
        localStorage.setItem(SAVE_KEY_SCORE, highScore);
    }

    // destroy the asteroid
    roids[index].destroyed = true;
    roids[index].explodeTime = 30;
    for (var i = 0; i < num_particles; i++)
    {
        roids[index].particles.push(new Particle(roids[index].x, roids[index].y));
    }
}

function distBetweenPoints(x1, y1, x2, y2)
{
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
}

function drawShip(x, y, angleRadians, colour = "white")
{
    ctx.strokeStyle = colour;
    ctx.lineWidth = SHIP_SIZE / 20;
    ctx.beginPath();
    ctx.moveTo( // nose of the ship
        x + 4 / 3 * ship.r * Math.cos(angleRadians),
        y - 4 / 3 * ship.r * Math.sin(angleRadians)
    );
    ctx.lineTo( // rear left
        x - ship.r * (2 / 3 * Math.cos(angleRadians) + Math.sin(angleRadians)),
        y + ship.r * (2 / 3 * Math.sin(angleRadians) - Math.cos(angleRadians))
    );
    ctx.arcTo( // rear right
        x - 1 / 6 * ship.r * Math.cos(angleRadians),
        y + 1 / 6 * ship.r * Math.sin(angleRadians),
        x - ship.r * (2 / 3 * Math.cos(angleRadians) - Math.sin(angleRadians)),
        y + ship.r * (2 / 3 * Math.sin(angleRadians) + Math.cos(angleRadians)),
        ship.r * 2
    );
    ctx.closePath();
    ctx.stroke();
}

function gameOver()
{
    ship.dead = true;
    text = "GAME OVER";
    textAlpha = 1.0;
}

function keyDown(/** @type {KeyboardEvent} */ ev) {

    if (ship.dead) {
        return;
    }

    switch (ev.keyCode) {
        case 32: // space bar (shoot laser)
            ship.shootLaser();
            break;
        case 37: // left arrow (rotate ship left)
            ship.rot = SHIP_TURN_SPD / 180 * Math.PI / FPS;
            break;
        case 38: // up arrow (thrust the ship forward)
            ship.thrusting = true;
            break;
        case 39: // right arrow (rotate ship right)
            ship.rot = -SHIP_TURN_SPD / 180 * Math.PI / FPS;
            break;
    }
}

function keyUp(/** @type {KeyboardEvent} */ ev) {

    if (ship.dead) {
        return;
    }

    switch (ev.keyCode) {
        case 32: // space bar (allow shooting again)
            ship.canShoot = true;
            break;
        case 37: // left arrow (stop rotating left)
            ship.rot = 0;
            break;
        case 38: // up arrow (stop thrusting)
            ship.thrusting = false;
            break;
        case 39: // right arrow (stop rotating right)
            ship.rot = 0;
            break;
    }
}

function newAsteroid(x, y, r)
{
    var lvlMult = 1 + 0.1 * level;
    var roid = {
        x: x,
        y: y,
        xv: Math.random() * ROID_SPD * lvlMult / FPS * (Math.random() < 0.5 ? 1 : -1),
        yv: Math.random() * ROID_SPD * lvlMult / FPS * (Math.random() < 0.5 ? 1 : -1),
        a: Math.random() * Math.PI * 2, // in radians
        r: r,
        offs: [],
        vert: Math.floor(Math.random() * (ROID_VERT + 1) + ROID_VERT / 2),
        particles: [],
        explodeTime: 0,
        destroyed: false
    };

    // populate the offsets array
    for (var i = 0; i < roid.vert; i++)
    {
        roid.offs.push(Math.random() * ROID_JAG * 2 + 1 - ROID_JAG);
    }

    return roid;
}

function newGame()
{
    level = 0;
    lives = GAME_LIVES;
    score = 0;
    ship = new Ship();

    // get the high score from local storage
    var scoreStr = localStorage.getItem(SAVE_KEY_SCORE);
    if (scoreStr == null)
    {
        highScore = 0;
    }
    else
    {
        highScore = parseInt(scoreStr);
    }

    newLevel();
}

function newLevel()
{
    text = "LEVEL " + (level + 1);
    textAlpha = 1.0;
    createAsteroidBelt();
}

//copyright for Atari
function drawCopyright()
{
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
    ctx.font = (TEXT_SIZE * 0.75) + "px Hyperspace";
    ctx.fillText(COPYRIGHT_MSG, canv.width / 2, canv.height * 0.9);
}

function update()
{
    var blinkOn = ship.blinkNum % 2 == 0;
    var exploding = ship.explodeTime > 0;

    // draw space
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canv.width, canv.height);

    // draw the asteroids
    var a, r, x, y, offs, vert;
    for (var i = 0; i < roids.length; i++)
    {
        ctx.strokeStyle = "slategrey";
        ctx.lineWidth = SHIP_SIZE / 20;

        // get the asteroid properties
        a = roids[i].a;
        r = roids[i].r;
        x = roids[i].x;
        y = roids[i].y;
        offs = roids[i].offs;
        vert = roids[i].vert;

        if (roids[i].destroyed)
        {
            for (var j = 0; j < num_particles; j++)
            {
                roids[i].particles[j].Update();
                roids[i].particles[j].Draw(ctx);
            }
            roids[i].explodeTime--;
        }
        else
        {
            // draw the path
            ctx.beginPath();
            ctx.moveTo(
                x + r * offs[0] * Math.cos(a),
                y + r * offs[0] * Math.sin(a)
            );

            // draw the polygon
            for (var j = 1; j < vert; j++) {
                ctx.lineTo(
                    x + r * offs[j] * Math.cos(a + j * Math.PI * 2 / vert),
                    y + r * offs[j] * Math.sin(a + j * Math.PI * 2 / vert)
                );
            }
            ctx.closePath();
            ctx.stroke();
        }

        // show asteroid's collision circle
        if (SHOW_BOUNDING) {
            ctx.strokeStyle = "lime";
            ctx.beginPath();
            ctx.arc(x, y, r, 0, Math.PI * 2, false);
            ctx.stroke();
        }
    }

    for (var i = roids.length - 1; i >= 0; i--)
    {
        if (roids[i].destroyed && roids[i].explodeTime <= 0)
        {
            roids.splice(i, 1);

            // new level when no more asteroids
            if (roids.length == 0)
            {
                level++;
                newLevel();
            }
        }
    }

    // thrust the ship
    if (ship.thrusting && !ship.dead)
    {
        ship.thrust.x += SHIP_THRUST * Math.cos(ship.a) / FPS;
        ship.thrust.y -= SHIP_THRUST * Math.sin(ship.a) / FPS;

        // draw the thruster
        if (!exploding && blinkOn)
        {
            ctx.strokeStyle = "white";
            ctx.lineWidth = SHIP_SIZE / 20;
            ctx.beginPath();
            ctx.moveTo( // rear left
                ship.x - ship.r * (2 / 3 * Math.cos(ship.a) + 0.5 * Math.sin(ship.a)),
                ship.y + ship.r * (2 / 3 * Math.sin(ship.a) - 0.5 * Math.cos(ship.a))
            );
            ctx.lineTo( // rear centre (behind the ship)
                ship.x - ship.r * 4 / 3 * Math.cos(ship.a),
                ship.y + ship.r * 4 / 3 * Math.sin(ship.a)
            );
            ctx.lineTo( // rear right
                ship.x - ship.r * (2 / 3 * Math.cos(ship.a) - 0.5 * Math.sin(ship.a)),
                ship.y + ship.r * (2 / 3 * Math.sin(ship.a) + 0.5 * Math.cos(ship.a))
            );
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
        }
    }
    else
    {
        // apply friction (slow the ship down when not thrusting)
        ship.thrust.x -= FRICTION * ship.thrust.x / FPS;
        ship.thrust.y -= FRICTION * ship.thrust.y / FPS;
    }

    // draw the triangular ship
    if (!exploding)
    {
        if (blinkOn && !ship.dead)
        {
            drawShip(ship.x, ship.y, ship.a);
        }

        // handle blinking
        if (ship.blinkNum > 0)
        {
            // reduce the blink time
            ship.blinkTime--;

            // reduce the blink num
            if (ship.blinkTime == 0)
            {
                ship.blinkTime = Math.ceil(SHIP_BLINK_DUR * FPS);
                ship.blinkNum--;
            }
        }
    }
    else
    {
        // draw the explosion (concentric circles of different colours)
        // ctx.fillStyle = "darkred";
        // ctx.beginPath();
        // ctx.arc(ship.x, ship.y, ship.r * 1.7, 0, Math.PI * 2, false);
        // ctx.fill();
        // ctx.fillStyle = "red";
        // ctx.beginPath();
        // ctx.arc(ship.x, ship.y, ship.r * 1.4, 0, Math.PI * 2, false);
        // ctx.fill();
        // ctx.fillStyle = "orange";
        // ctx.beginPath();
        // ctx.arc(ship.x, ship.y, ship.r * 1.1, 0, Math.PI * 2, false);
        // ctx.fill();
        // ctx.fillStyle = "yellow";
        // ctx.beginPath();
        // ctx.arc(ship.x, ship.y, ship.r * 0.8, 0, Math.PI * 2, false);
        // ctx.fill();
        // ctx.fillStyle = "white";
        // ctx.beginPath();
        // ctx.arc(ship.x, ship.y, ship.r * 0.5, 0, Math.PI * 2, false);
        // ctx.fill();

        for (var i = 0; i < ship.numParticles; i++)
        {
            ship.particles[i].Update();
            ship.particles[i].Draw(ctx);
        }
    }

    // show ship's collision circle
    if (SHOW_BOUNDING)
    {
        ctx.strokeStyle = "lime";
        ctx.beginPath();
        ctx.arc(ship.x, ship.y, ship.r, 0, Math.PI * 2, false);
        ctx.stroke();
    }

    // show ship's centre dot
    if (SHOW_CENTRE_DOT)
    {
        ctx.fillStyle = "red";
        ctx.fillRect(ship.x - 1, ship.y - 1, 2, 2);
    }

    // draw the lasers
    for (var i = 0; i < ship.lasers.length; i++)
    {
        if (ship.lasers[i].explodeTime == 0)
        {
            ctx.fillStyle = "white";
            ctx.beginPath();
            ctx.arc(ship.lasers[i].x, ship.lasers[i].y, SHIP_SIZE / 15, 0, Math.PI * 2, false);
            ctx.fill();
        }
    }

    // draw the game text
    if (textAlpha >= 0)
    {
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillStyle = "rgba(255, 255, 255, " + textAlpha + ")";
        ctx.font = "small-caps " + TEXT_SIZE + "px Hyperspace";
        ctx.fillText(text, canv.width / 2, canv.height * 0.75);
        textAlpha -= (1.0 / TEXT_FADE_TIME / FPS);
    }
    else if (ship.dead)
    {
        // after "game over" fades, start a new game
        newGame();
    }

    //copyright
    drawCopyright();

    // draw the lives
    var lifeColour;
    for (var i = 0; i < lives; i++)
    {
        lifeColour = exploding && i == lives - 1 ? "white" : "white";
        drawShip(SHIP_SIZE + i * SHIP_SIZE * 1.2, SHIP_SIZE, 0.5 * Math.PI, lifeColour);
    }

    // draw the score
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
    ctx.font = "small-caps " + TEXT_SIZE + "px Hyperspace";
    ctx.fillText(score, canv.width - SHIP_SIZE / 2, SHIP_SIZE);

    // draw the high score
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
    ctx.font = (TEXT_SIZE * 0.75) + "px Hyperspace";
    ctx.fillText("HIGH " + highScore, canv.width / 2, SHIP_SIZE);

    // detect laser hits on asteroids
    var ax, ay, ar, lx, ly;
    for (var i = roids.length - 1; i >= 0; i--) {

        // grab the asteroid properties
        ax = roids[i].x;
        ay = roids[i].y;
        ar = roids[i].r;

        // loop over the lasers
        for (var j = ship.lasers.length - 1; j >= 0; j--) {

            // grab the laser properties
            lx = ship.lasers[j].x;
            ly = ship.lasers[j].y;

            // detect hits
            if (!roids[i].destroyed && ship.lasers[j].explodeTime == 0 && distBetweenPoints(ax, ay, lx, ly) < ar)
            {
                // destroy the asteroid and activate the laser explosion
                destroyAsteroid(i);
                ship.lasers[j].explodeTime = Math.ceil(LASER_EXPLODE_DUR * FPS);
                break;
            }
        }
    }

    // check for asteroid collisions (when not exploding)
    if (!exploding)
    {
        // only check when not blinking
        if (ship.blinkNum == 0 && !ship.dead)
        {
            for (var i = 0; i < roids.length; i++)
            {
                if (!roids[i].destroyed && ship.collisionDetected(roids[i]))/*(distBetweenPoints(ship.x, ship.y, roids[i].x, roids[i].y) < ship.r + roids[i].r)*/
                {
                    ship.explode();
                    destroyAsteroid(i);
                    break;
                }
            }
        }

        // rotate the ship
        ship.a += ship.rot;

        // move the ship
        ship.x += ship.thrust.x;
        ship.y += ship.thrust.y;
    }
    else
    {
        // reduce the explode time
        ship.explodeTime--;

        // reset the ship after the explosion has finished
        if (ship.explodeTime == 0)
        {
            lives--;
            if (lives == 0)
            {
                gameOver();
            } else
            {
                ship = new Ship();
            }
        }
    }

    // handle edge of screen
    if (ship.x < 0 - ship.r)
    {
        ship.x = canv.width + ship.r;
    }
    else if (ship.x > canv.width + ship.r)
    {
        ship.x = 0 - ship.r;
    }
    if (ship.y < 0 - ship.r)
    {
        ship.y = canv.height + ship.r;
    }
    else if (ship.y > canv.height + ship.r)
    {
        ship.y = 0 - ship.r;
    }

    // move the lasers
    for (var i = ship.lasers.length - 1; i >= 0; i--)
    {
        // check distance travelled
        if (ship.lasers[i].dist > LASER_DIST * canv.width)
        {
            ship.lasers.splice(i, 1);
            continue;
        }

        // handle the explosion
        if (ship.lasers[i].explodeTime > 0)
        {
            ship.lasers[i].explodeTime--;

            // destroy the laser after the duration is up
            if (ship.lasers[i].explodeTime == 0)
            {
                ship.lasers.splice(i, 1);
                continue;
            }
        }
        else
        {
            // move the laser
            ship.lasers[i].x += ship.lasers[i].xv;
            ship.lasers[i].y += ship.lasers[i].yv;

            // calculate the distance travelled
            ship.lasers[i].dist += Math.sqrt(Math.pow(ship.lasers[i].xv, 2) + Math.pow(ship.lasers[i].yv, 2));
        }

        // handle edge of screen
        if (ship.lasers[i].x < 0)
        {
            ship.lasers[i].x = canv.width;
        }
        else if (ship.lasers[i].x > canv.width)
        {
            ship.lasers[i].x = 0;
        }
        if (ship.lasers[i].y < 0)
        {
            ship.lasers[i].y = canv.height;
        }
        else if (ship.lasers[i].y > canv.height)
        {
            ship.lasers[i].y = 0;
        }
    }

    // move the asteroids
    for (var i = 0; i < roids.length; i++)
    {
        roids[i].x += roids[i].xv;
        roids[i].y += roids[i].yv;

        // handle asteroid edge of screen
        if (roids[i].x < 0 - roids[i].r)
        {
            roids[i].x = canv.width + roids[i].r;
        }
        else if (roids[i].x > canv.width + roids[i].r)
        {
            roids[i].x = 0 - roids[i].r
        }
        if (roids[i].y < 0 - roids[i].r)
        {
            roids[i].y = canv.height + roids[i].r;
        }
        else if (roids[i].y > canv.height + roids[i].r)
        {
            roids[i].y = 0 - roids[i].r
        }
    }
}
